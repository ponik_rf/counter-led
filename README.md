# Counter Unit Led

Плата индикации для [контроллера счетчика импульсов](https://gitlab.com/ponik_rf/counter-controller)

![Основная схема](specification/Scheme.svg)

* [Используемые порты](specification/controller.png) - Используемые порты контроллера

## Сборка проекта

Сборка проекта осуществляется аналогично сборке [Основного контроллера](https://gitlab.com/ponik_rf/counter-controller)

## Hardware

### Плата

Плата разработана в программе SprintLayout6, [найти ее можно тут](/layout/led_layout2.lay6). Плата после сброки и прошивки устанавливается на порт
расширения осовной платы контроллера

![Фронт](/layout/front.jpg)
![Бэк](/layout/back.jpg)
